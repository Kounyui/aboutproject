from django.apps import AppConfig


class AfricanregionsConfig(AppConfig):
    name = 'africanregions'
